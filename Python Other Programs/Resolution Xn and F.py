# -*- coding: utf-8 -*-
"""
Created on Mon Jan  4 14:05:19 2021

@author: tesemine
"""

import numpy as np
import matplotlib.pyplot as plt

#----Variables----#

m = 50      #Mouving mass applied to the system
Mw = 15  #Mass applied to calibrate
Mt = 200        #Mass of the thruster
Mp = 200        #Mass of the plateform

#----Xn----#
Xn_max = 400
Xn_precision = 0.1
Xn = np.arange(0, Xn_max, Xn_precision)
Xn_found = np.zeros(15)

#----Xt----#
Xt_max = 10
Xt_precision = 1
Xt = np.arange(0, Xt_max, Xt_precision)

#----Lenght---#
L = 350
E = 400
a = 250
H = 250

#----g----#
g = 9.81

#----Thrust----#
F_found = np.zeros(int(Xn_max/Xn_precision))
F_list = np.arange(0,150,10)

#----Alpha----#
alpha = np.zeros(int(Xn_max/Xn_precision))

#----Gamma----#
gamma = 70

#----Teta----#
teta = np.arcsin(Xt/H)


def FtoXn():
    for F in F_list:
        x = np.zeros(len(teta))
        for k in range(len(teta)):
            x[k] = F + (np.sin(teta[k])/np.cos(teta[k]))*(m*g/2 + Mw*g/2 + Mt*g + Mp*g) + (Mw*g*np.cos(gamma))/(2*np.sin(gamma))
            x[k] = x[k]*2/(m*g)
            x[k] = 1/x[k]
        global alpha
        alpha = np.arctan(x)
        global Xn_found
        Xn_found = 2*a*np.cos(alpha) + E - Xt - L
        
        plt.plot(Xn_found, Xt)
        
def XntoF():
    for x in range(len(Xt)):
        global alpha
        #print((Xn + Xt[x] + L - E)/(2*a))
        alpha = np.arccos((Xn + Xt[x] + L - E)/(2*a))
        global F_found
        F_found = (m*g/2)*(np.cos(alpha)/np.sin(alpha) - np.sin(teta[x])/np.cos(teta[x]))
        F_found -= (Mw*g/2)*(np.cos(alpha)/np.sin(alpha) + np.sin(teta[x])/np.cos(teta[x]))
        F_found -= (np.sin(teta[x])/np.cos(teta[x]))*(Mp*g + Mt*g)
        
        plt.plot(Xn, F_found)