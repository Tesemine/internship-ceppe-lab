# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt


def main():
    """
    Created on Wed Oct  7 09:34:49 2020
    Last Update on Tue Oct 10 2020
    
    @author: tesemine
    
    Programm for estimating Xn to have Xt = 0
    This applied for the resolution of the static equations linking Xn, Xt, F and m.
    To resolve the equation, the program test a range of Xn to find which one works.
    For a given range of thrust and mass, the programm return :
        - a representation of the member of the equation to null
        - a representation of the Xn needed depending of the ration between the F applied and the m used, for diffrent F.
    """
    
    
    #---------Variables------------
    
    #---Masses
    
    g = 9.81
    #Mass added in gramm
    mass_max = 100                                                  #range of the mass tested
    mass_prec = 5                                                   #step between each tested mass
    mass = np.arange( 0, mass_max, mass_prec, dtype = float)        #the number of mass tested is mass_max/mass_prec
    #Mass of the components in g/m
    rhoL = 0.1
    
    #---Lenght in mm
    a = 240                                                        #Lenght of the rope
    b = a                                                           #Lenght of the second rope, taken equal to the first one
    h = 20                                                          #High of the attachment point of the rope compare to the plate
    l = 350                                                         #Lenght of the plate
    
    #Thrust applied in mN
    Thrust_max = 150                                                #Thrust max that will be tested in the resolution
    Thrust_prec = 15                                                #Step between each tested thrust
    Thrust = np.arange( 0, Thrust_max, Thrust_prec)
    
    #---Xn in mm
    Xn_max = 400                                                    #Xn max authorised by the real systeme
    Xn_prec = 0.1                                                   #Precision of the Xn tested
    Xn = np.arange( Xn_max, 0, -Xn_prec, dtype=float)
    
    #---Angles
    alpha = np.zeros(int(Xn_max/Xn_prec))
    beta = np.zeros(int(Xn_max/Xn_prec))
    
    
    
    #----------Main--------------
    
    #Angle calculation
    alpha = 0.5 * (np.arccos(((l-Xn)*(l-Xn) + h**2)/(2*a**2)) + np.arccos(((l-Xn)*(l-Xn) - h**2)/(2*a**2)))
    beta = 0.5 * (np.arccos(((l-Xn)*(l-Xn) + h**2)/(2*a**2)) - np.arccos(((l-Xn)*(l-Xn) - h**2)/(2*a**2)))
    
    #Equation resolution
    #plt.subplot(2,1,1)
    M = np.zeros(200)
    Recap = []  #Will be used to save the different solutions
    
    for F in Thrust:
        save = []
        for m in mass:
            
            M = - rhoL*a**2*0.5*g*np.cos(alpha) - rhoL*b*g*(b*0.5*np.cos(beta) + a*np.cos(alpha)) - a*m*g*np.cos(alpha)
        
            Total = M + (Xn-l)*g*(-0.5*rhoL*a - rhoL*b - m) + F*h
            
            #plt.plot(Xn, Total)                                     #Plooting of the courb for this F and m
            
            
            mini = 0
            for k in range(1,int(Xn_max/Xn_prec-1)):
                if abs(Total[k]) < abs(Total[k+1]) and abs(Total[k]) < abs(Total[k-1]):
                    mini = k
            save += [[m,Xn[mini]]] 
            
        #F is the Thrust tested this Loop
        #Save is composed of a list of all the Xn, for each m applied, for the given F
        Recap += [[F,save]] 
        
    #Display of the first graph
    #plt.title('Total compared to Xn')
    #plt.xlabel('Xn')
    #plt.ylabel('Total')
    
    #Resuming the results
    #plt.subplot(1,1,2)
    F_m = []
    Xn_sol = []
    for r in Recap:
        for m in r[1]:
            if r[0] != 0:
                F_m += [m[0]/r[0]]
                Xn_sol += [380 - m[1]]
                
    
    coef = int(mass_max/mass_prec)

    for k in range(coef):
        plt.plot(Xn_sol[k::coef], F_m[k::coef], label = str(mass[k]))
    plt.legend()
    
    for k in range(len(Thrust)):
        plt.plot(Xn_sol[0+k*coef:coef+k*coef],F_m[0+k*coef:coef+k*coef], label = str(Thrust[k]), color = 'black')
    
    
    #Display of the results
    plt.title('m/F compared to the Xn needed')
    plt.xlabel('Xn needed')
    plt.ylabel('m/F')
    print(0)
    plt.show()

