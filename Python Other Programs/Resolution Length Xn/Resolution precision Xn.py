# -*- coding: utf-8 -*-

import numpy as np


def main():
    """
    Created on Wed Oct 14 12:08:15 2020
    
    @author: tesemine
    
    Programm to estimate the precision needed on Xn
    When called, return the F precision achieved with the given precisions
    asked in the variables.
    """
    
    #---------Variables------------
    
    #---Masses
    
    g = 9.81
    #Mass added in gramm
    m = 10        
    dm = 0.001
    #Mass of the components in g/m
    rhoL = 0.1
    
    #---Lenght in mm
    a = 250                                     #Lenght of the rope
    da = 0.05
    b = a                                       #Lenght of the second rope, taken equal to the first one
    h = 70                                     #High of the attachment point of the rope compare to the plate
    dh = 0.05
    l = 350                                     #Lenght of the plate
    dl = 0.05
    
    #Thrust applied in mN
    Thrust = 100
    
    #---Xn in mm
    Xn = 300
    dXn = 0.001
    
    #---Xt in mm
    Xt = 1
    dXt = 0.00001
    
    #---k in N/mm
    k = 0.1
    dk = 0.0001
    
    #---Angles
    alpha = 0.5 * (np.arccos(((l-Xn)*(l-Xn) + h**2)/(2*a**2)) + np.arccos(((l-Xn)*(l-Xn) - h**2)/(2*a**2)))
    beta = 0.5 * (np.arccos(((l-Xn)*(l-Xn) + h**2)/(2*a**2)) - np.arccos(((l-Xn)*(l-Xn) - h**2)/(2*a**2)))
    
    
    #----------Main-----------
    df_dh1 = k*Xt**2*np.sin(alpha)/(h**2*np.cos(alpha))
    df_dh2 = -Xt*k*(Xn-l)*np.sin(alpha)/(h**2*np.cos(alpha))
    df_dh3 = 3*Xt*rhoL*g*a/(2*h**2)
    df_dh4 = Xt*m*g/h**2
    df_dh5 = -rhoL*3*a**2*g*np.cos(alpha)/(2*h**2)
    df_dh6 = -rhoL*a**2*g*np.cos(beta)/(2*h**2)
    df_dh7 = -a*m*g*np.cos(alpha)/h**2
    df_dh8 = -rhoL*3*a*g*(Xn - l)/(2*h**2)
    df_dh8 = -m*g*(Xn - l)/(2*h**2)
    
    df_dh = abs(df_dh1 + df_dh2 + df_dh3 + df_dh4 + df_dh5 + df_dh6 + df_dh7 + df_dh8)
    print('Pour h ', df_dh*dh)
    
    
    df_da1 = -3*Xt*k*np.sin(alpha)/(2*h*np.cos(alpha))
    df_da2 = 3*rhoL*a*g*np.cos(alpha)/h
    df_da3 = rhoL*a*g*np.cos(beta)/h
    df_da4 = m*g*np.cos(alpha)/h
    df_da5 = 3*rhoL*g*(Xn-l)/(2*h)
    
    df_da = abs(df_da1 + df_da2 + df_da3 + df_da4 + df_da5)
    print('Pour a ', df_da*da)
    
    df_dl1 = -Xt*k*np.sin(alpha)/(h*np.cos(alpha))
    df_dl2 = -3*a*rhoL*g/(2*h)
    df_dl3 = -m*g/h
    
    df_dl = abs(df_dl1 + df_dl2 + df_dl3)
    print('Pour l ', df_dl*dl)
    
    df_dm1 = -Xt*g/h
    df_dm2 = a*g*np.cos(alpha)/h
    df_dm3 = g*(Xn-l)/h
    
    df_dm = abs(df_dm1 + df_dm2 + df_dm3)
    print('Pour m ', df_dm*dm)
    
    df_dXn1 = Xt*k*np.sin(alpha)/(h*np.cos(alpha))
    df_dXn2 = 3*a*rhoL*g/(2*h)
    df_dXn3 = m*g/h
    
    df_dXn = abs(df_dXn1 + df_dXn2 + df_dXn3)
    print('Pour Xn ', df_dXn*dXn)
    
    df_dXt1 = -2*k*Xt*np.sin(alpha)/(h*np.cos(alpha))
    df_dXt2 = k
    df_dXt3 = k*(Xn-l)*np.sin(alpha)/(h*np.cos(alpha))
    df_dXt4 = -3*a*rhoL*g/(2*h)
    df_dXt5 = -m*g*h
    
    df_dXt = abs(df_dXt1 + df_dXt2 + df_dXt3 + df_dXt4 + df_dXt5)
    print('Pour Xt ', df_dXt*dXt)
    
    df_dk1 = - Xt**2*np.sin(alpha)/(h*np.cos(alpha))
    df_dk2 = Xt
    df_dk3 = Xt*(Xn-l)*np.sin(alpha)/(h*np.cos(alpha))
    
    df_dk = abs(df_dk1 + df_dk2 + df_dk3)
    print('Pour k ', df_dk*dk)
    
    
    #dThrust = df_dh*dh + df_da*da + df_dl*dl + df_dm*dm + df_dXt*dXt + df_dk*dk + df_dXn*dXn
    dThrust =  df_dXt*dXt + df_dXn*dXn
    print("This version only take Xn and Xt in account")
    #print(-(- df_dh*dh - df_da*da - df_dl*dl - df_dm*dm - df_dXt*dXt - df_dk*dk))
    #print(df_dh*dh + df_da*da + df_dl*dl + df_dm*dm + df_dXt*dXt + df_dk*dk + df_dXn*ddXn)
    return(dThrust)
    