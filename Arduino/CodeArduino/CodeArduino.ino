//Pin motor nulling system
#define enablePin 4
#define stepPin 3
#define dirPin 2
#define homePin1 5
#define endPin1 6


// Values shown are the default values
// They might be overwritten by the python program

String identification_string = "Arduino";

float location = 0.0; //current location
//float location_list[200];
//long i_loclst = 0;
float target_location = 0.0;  //location to reach
float home_speed = 20000; // step per second
float home_return = 3600; // deg to return after hitting switch
float set_speed = 0; // Speed Number int (from 0=Null to 19=Max)
float target_speed = 0.0; // = 2^(set_speed-1)
bool dir = true; //true = clockwise, false = counterclockwise
//The number of steps is 6400/tr
float distance_per_revolution = 6; // in mm (Screw thread)
float step_per_revolution = 6400; //number of step indicated on the driver
float stepsize_degrees = 360.0/step_per_revolution; //degree of one step
float stepsize_distance = (distance_per_revolution * stepsize_degrees) / 360; //distance of one step
String input_text; //text received by the card
bool homed = false; //indicate if the carriage is homed
bool ended = false; //indicate if the carriage is at the end
bool stopped = false; //indicate if the system has been stopped by user
bool paused = false;  //indicate if the system has been paused by user
float step_count = 0.0; //keep count of the number of step theoricaly donne since the last reset
long target_step = 0; //number of step to do when in runloc mod
String cmd_buffer[10];  //buffer of alien command enter during a run
String cmd_read = ""; //text recieve during a run
int f_buffer = -1;  //tracker of the buffer
int x;  //random number for the loops

//Debouncing stuff for home switch
int homeSwitchState;
int lastHomeSwitchState = HIGH;
//Debouncing stuff for End switch
int endSwitchState;
int lastEndSwitchState = HIGH;
//Other debouncing stuff
unsigned long lastDebounceTime = 0;
unsigned long debounceDelay = 50;



void setup() {
  Serial.begin(19200);
  Serial.setTimeout(100); // Default timeout is 1 second = 1000 ms

  //Setup motor nulling system
  pinMode(enablePin, OUTPUT);
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(homePin1, INPUT);
  pinMode(endPin1, INPUT);

  //Setup motor calibration system
  
  resetPins();
}

void loop() {

  delay(100);
  
  if (Serial.available() || f_buffer > -1) {
    if (f_buffer > -1){
      input_text = cmd_buffer[f_buffer];
      f_buffer--;
    }
    else{
      input_text = Serial.readString();
    }
    //Serial.println(input_text);
    input_text.trim();
    if (input_text == ""){
      Serial.println("No command");
    }
    else if (input_text == "*IDN?"){
      Serial.println(identification_string);
    }
    else if (input_text == "loc") {
      Serial.println(location, 6);
    }
    else if (input_text == "loclst"){
      //sendLocLst();
      Serial.println("Done");
    }
    else if (input_text.startsWith("loc")) {
      input_text.remove(0,3);
      location = input_text.toFloat();
      Serial.println(location);
    }
    else if (input_text == "tloc") {
      Serial.println(target_location);
    }
    else if (input_text.startsWith("tloc")) {
      input_text.remove(0,4);
      target_location = input_text.toFloat();
      Serial.println("Done");
    }
    else if (input_text == "tstep") {
      Serial.println(target_step);
    }
     else if (input_text.startsWith("tstep")) {
      input_text.remove(0,5);
      target_step = input_text.toInt();
      Serial.println("Done");
    }
    else if (input_text == "rstloc") {
      location = 0.0;
      Serial.println(location);
    }
    else if (input_text == "hs"){
      Serial.println(home_speed);
    }
    else if (input_text == "hr"){
      Serial.println(home_return);
    }
    else if (input_text.startsWith("hs")) {
      input_text.remove(0,2);
      home_speed = input_text.toFloat();
      Serial.println("Done");
    }
    else if (input_text.startsWith("hr")) {
      input_text.remove(0,2);
      home_return = input_text.toFloat();
      Serial.println("Done");
    }
    else if (input_text == "s") {
      Serial.println(set_speed);
    }
    else if (input_text.startsWith("s")) {
      input_text.remove(0,1);
      if (input_text.toInt() <= 39 && input_text.toInt() >= 0) {
              set_speed = input_text.toFloat();
      Serial.println("Done");
      }
      else{
        Serial.println("ErrorSpeedOutOfRange");
      }
    }
    else if (input_text == "dpr") {
      Serial.println(distance_per_revolution);
    }
    else if (input_text == "idleinfo") {
      if (homed) {
        Serial.println("Homed,");
      }
      else {
        Serial.println("NoHomed,");
      };
      if (ended) {
        Serial.println("Ended,");
      }
      else {
        Serial.println("NoEnded,");
      };
      Serial.println("");
    }
    else if (input_text == "runcal"){
      runMotorCal();
      //Serial.println("Done");
    }
     else if (input_text == "runloc"){
      runMotorLoc();
      //Serial.println("Done");
    }
    else if (input_text == "reset"){
      reset();
      Serial.println("Done");
    }
    else if (input_text == "dir"){
      Serial.println(dir);
    }
    else if (input_text == "dirs"){
      dir = !dir;
      Serial.println("Done");
    }
    else{
      Serial.println("ErrorCmdUnknown");
    };
  }
}
///////////////////////////////////////////////////////////////////////
/*void addLoc(){
  if (i_loclst == 200){
    sendLocLst();
  }
  location_list[i_loclst] = location;
  i_loclst++;
}*/
///////////////////////////////////////////////////////////////////////
bool checkDir(){
  //cant go further than the switches
  if (dir && ended){
    return true;
  }
  else if (!dir && homed){
    return true;
  }
  //if can go, not ended nor homed anymore
  else{
    homed = false;
    ended = false;
    return false;
  }
}
////////////////////////////////////////////////////////////////////////
void cmdInput(int i){
  if (Serial.available()){
    cmd_read = Serial.readString();
    cmd_read.trim();
    //Serial.println(cmd_read);
    if (cmd_read == "STOP"){
      stopped = true;
      Serial.println("Stopped");
    }
    else if (cmd_read == "dirs" && i == 1){
      dir = !dir;
      if (dir){
       digitalWrite(dirPin, HIGH);
      }
      else if (!dir){
        digitalWrite(dirPin, LOW);
      }
      Serial.println("Done");
    }
    else if (cmd_read.startsWith("s") && i == 1){
      cmd_read.remove(0,1);
      set_speed = cmd_read.toFloat();
      setTargetSpeed();
      Serial.println("Done");
    }
    else if (cmd_read == "loc"){
      Serial.println(location, 6);
    }
    else if (cmd_read == "PAUSE"){
      paused = true;
      Serial.println("Paused");
    }
    else if (cmd_read == "RESUME"){
      if (paused){
        paused = false;
        Serial.println("Resumed");
      }
    }
    else if (cmd_read != ""){
      if (f_buffer < 15){
        f_buffer += 1;
        cmd_buffer[f_buffer] = cmd_read;
      }
      else{
        Serial.println("ErrorBUFFERFULL");
      }
    }
  }
}
////////////////////////////////////////////////////////////////////////////////
void isEnded(){
  int reading = digitalRead(endPin1);
  if(reading != lastEndSwitchState){
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    if (reading != endSwitchState){
      endSwitchState = reading;
      if (endSwitchState == LOW){
        ended = true;
      }
    }
  }
  lastEndSwitchState = reading;
}
//////////////////////////////////////////////////////////////////////////////
void isHomed(){
  int reading = digitalRead(homePin1);
  if(reading != lastHomeSwitchState){
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    if (reading != homeSwitchState){
      homeSwitchState = reading;
      if (homeSwitchState == LOW){
        homed = true;
      }
    }
  }
  lastHomeSwitchState = reading;
}
///////////////////////////////////////////////////////////////////////////////
void reset(){

  homed = false;
  ended = false;
  digitalWrite(dirPin, LOW);

  //Check for ability to go in this direction
  if (checkDir()){
    Serial.println("ErrorOutSwitch");
    return;
  }

  while (!homed && !ended && !stopped){
    //Serial.println("moving");
    digitalWrite(stepPin, LOW);
    delayMicroseconds(25);
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(25);
    isHomed();
    isEnded();
    cmdInput(0);
  }

  if (homed){
    runSwitch();
    Serial.println("Homed");
    location = 0.0;
    digitalWrite(dirPin, HIGH);
    return;
  }

  if (ended){
    runSwitch();
    Serial.println("ErrorSideReset");
    digitalWrite(dirPin, HIGH);
    return;
  }
  
}
/////////////////////////////////////////////////////////////////////////////
void resetPins(){
  digitalWrite(stepPin, HIGH);
  digitalWrite(dirPin, HIGH);
  digitalWrite(enablePin, HIGH);
}
////////////////////////////////////////////////////////////////////////////////
void runMotorLoc(){

  //Checking for a valid target location
  if (target_location != location && target_step == 0){
    setTargetStep();
  }
  else if (target_step == 0){
    Serial.println("ErrorNoTarget");
    return;
  }

  //Set direction
  if (target_step > 0){
    digitalWrite(dirPin, HIGH);
    dir = true;
  }
  else if (target_step < 0){
    digitalWrite(dirPin, LOW);
    dir = false;
  }

  //Check for ability to go in this direction
  if (checkDir()){
    Serial.println("ErrorOutSwitch");
    return;
  }
  
  //Choice of the speed
  if (set_speed == 0){
    Serial.println("ErrorNoSpeed");
    return; 
  }
  else{
    setTargetSpeed();
  }

  //Running the motor
  step_count = 0;
  while (!homed && !ended && !stopped && step_count != target_step){
    if (!paused){
      //Serial.println("moving");
      digitalWrite(stepPin, LOW);
      if (target_speed == 1){
        delay(800);
      }
      else{
        delayMicroseconds(1000000/target_speed);
        //delayMicroseconds(200);
      }
      digitalWrite(stepPin, HIGH);
      if (target_speed == 1){
        delay(800);
      }
      else{
        delayMicroseconds(1000000/target_speed);
        //delayMicroseconds(200);
      }
      //delayMicroseconds(200);
      isHomed();
      isEnded();
      
       //counting the steps and location
      if (dir){
        step_count++;
        location = step_count*stepsize_distance;
      }
      else if (!dir){
        step_count--;
        location = step_count*stepsize_distance;
      }
    }
    cmdInput(0);

    //addLoc();
  }

  //if homed or ended
  if (homed){
    runSwitch();
    Serial.println("Homed");
    return;
  }
  if (ended){
    runSwitch();
    Serial.println("Ended");
    return;
  }

  //Reset parametres
  stopped = false;
  target_location = location;
  target_step = 0;


  //Send the result back
  Serial.println("Done");
  Serial.println(location);
}
////////////////////////////////////////////////////////////////////////////////////
void runMotorCal(){
  //Check for ability to go in this direction
  if (checkDir()){
    Serial.println("ErrorOutSwitch");
    return;
  }
  
  //Set direction
  if (dir){
    digitalWrite(dirPin, HIGH);
  }
  else if (!dir){
    digitalWrite(dirPin, LOW);
  }

  //Choice of the speed
  if (set_speed == 0){
    Serial.println("ErrorNoSpeed");
    return; 
  }
  else{
    setTargetSpeed();
  }

  Serial.println("Running");
  
  //Running the motor
  step_count = 0;
  while (!homed && !ended && !stopped){
    if (!paused){
      //Serial.println("moving");
      digitalWrite(stepPin, LOW);
      if (target_speed == 1){
        delay(100);
      }
      else{
        delayMicroseconds(1000000/target_speed);
        //delayMicroseconds(200);
      }
      digitalWrite(stepPin, HIGH);
     if (target_speed == 1){
        delay(100);
      }
      else{
        delayMicroseconds(1000000/target_speed);
        //delayMicroseconds(200);
      }
      //delayMicroseconds(200);
      isHomed();
      isEnded();
  
      //counting the steps and location
      if (dir){
        step_count++;
        location = step_count*stepsize_distance;
      }
      else if (!dir){
        step_count--;
        location = step_count*stepsize_distance;
      }
      //Serial.println(location);
      //addLoc();
    }
    cmdInput(1);
  }

  //if homed or ended
  if (homed){
    runSwitch();
    Serial.println("Homed");
    return;
  }
  if (ended){
    runSwitch();
    Serial.println("Ended");
    return;
  }

  //Reset parametres
  stopped = false;


  //Send the result back
  Serial.println("Done");
  Serial.println(location);
}
/////////////////////////////////////////////////////////////////////////////
void runSwitch(){
  //Serial.print("switched");
  if (homed){
    digitalWrite(dirPin, HIGH);
  }
  if (ended){
    digitalWrite(dirPin, LOW);
  }

  for(long x=0; x<6400*home_return/360; x++){
    digitalWrite(stepPin, LOW);
    delayMicroseconds(1000000/(home_speed*2));
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(1000000/(home_speed*2));
  }
}
/////////////////////////////////////////////////////////////////////////////
/*void sendLocLst(){
  for (x=0;x<=i_loclst;x++){
    Serial.println(location_list[x], 5);
    location_list[x] = 0.0;
  }
  Serial.println("EndList");
  i_loclst = 0;
}*/
/////////////////////////////////////////////////////////////////////////////
void setTargetSpeed(){
  if (set_speed == 1){
    target_speed = 1;
  }
  else{
    target_speed = (unsigned int)pow(2,set_speed - 1);
    //Serial.println(target_speed);
  }
}
/////////////////////////////////////////////////////////////////////////////
void setTargetStep(){
  target_step = (target_location - location)/stepsize_distance;
}
/////////////////////////////////////////////////////////////////////////////
