# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 11:28:11 2020

@author: tesemine

file containing all the fonction needed in the programm.
"""
#import pyvisa as pv
import nidaqmx as nx
import numpy as np
import tkinter as tk
import time
import datetime

import lib.comard as cm
import lib.config as cf
import lib.comcal as ca
#import lib.comlv as cl
import run
import lib.nulllvdt as nl
import lib.test as test

import openpyxl as xl


class functionInstr():
    
    
    """
    Create all the ressources need to control and to communicate with the system.
    It is the heart of the system, behind the GUI.
    Contain the folloing methods:
    |     __init__(self)
    |         - Create all the variables usefull for the programm
    |         
    |    getInstr(self)
    |        - Return the opened resource currently configure
    |    
    |    getRmList(self)
    |        - Return the list of all the resources founded during configuration
    |        
    |    nullinglvdt(self)
    |        - Set the null position of the LVDT. Call lib.nulllvdt.
    |        
    |    resetArduino(self)
    |        - Reset the Arduino motor position.
    |        
    |    resetCal(self)
    |        - Reset the Calibration motor position.
    |        
    |    testSys(self)
    |        - Test all the system
    |        
    |    runSysCal(self)
    |        - Run the calibration programm of the system
    |        
    |    closeResource(self)
    |        - Close the task
        
    """
    
    
    def __init__(self):
        
        """Create all the variables usefull for the programm"""
        
        self.ok = True
        self.res = response()
        self.mass_count = ca.stepCal()
        self.loclst = locationlst()
        
        conf = cf.config(self.res)
        conf.mainloop()
        
        
        self.rm = conf.rm
        self.rm_list = conf.rmlist
        
        if not self.res.res2:
            try:
                self.arduinoName = conf.strvar[0]
                self.arduino = self.rm.open_resource(self.arduinoName)
                self.arduino.baud_rate = 19200 #Must be the same as Device Manager and Arduino Card
                self.arduino.read_termination = '\r\n'
                
            except:
                with open("lib\Error.txt", "a") as txt:
                    txt.write("Error\tUnable to find the Arduino Card")
                    self.ok = False
            
            try:
                self.calName = conf.strvar[1]
                self.calmotor = self.rm.open_resource(self.calName)
                #self.calmotor.read_termination = '\r\n'
            except:
                with open("lib\Error.txt", "a") as txt:
                    txt.write("Error\tUnable to find the Calibration Motor")
                    self.ok = False
                
            try:
                self.lvdtName = conf.lvdt
                self.task = nx.Task()
                self.task.ai_channels.add_ai_voltage_chan(self.lvdtName)
            except:
                with open("lib\Error.txt", "a") as txt:
                    txt.write("Error\tUnable to find the LVDT")
                    self.ok = False
        
            if self.ok and (not self.res.res3):
                #print(0)
                self.testSys()
            
                if (not self.res.res1):
                    self.ok = False
                
            
            try:
                self.datasheet = dataSheet(self.arduino)
            except:
                with open("lib\Error.txt", "a") as txt:
                    txt.write("Error\tUnable to Collect data")
                    self.ok = False
            
            self.temp = run.temp()
                
                
        else:
            self.ok = False
            
            
    def closeResource(self):
        
        """Close the Task"""
        
        #We clean both buffer
        try:
            self.arduino.query("")
        except:
            pass
        empty = False
        while not empty:
            try:
                self.arduino.read()
            except:
                empty = True
        try:
            self.arduino.clear()
            self.calmotor.clear()
        except:
             pass  
        #We close the resources
        try:
            self.task.close()
            self.arduino.close()
            self.calmotor.close()
            self.rm.close()
        except:
            pass
        #We reset the temporary files
        with open("temp/TempCount.txt", "w") as txt:
            txt.write("")
            

    def exportData(self, name = None, many = None):
        
        """Export the Data in an excel file"""
        
        if name:
            path = name
            
            wb = xl.Workbook()
            if not many:
                w_sheet = wb.create_sheet("Sheet 1")
                
                w_sheet['A1'] = "Data from the Nulling System"
                w_sheet['A2'] = "Time"
                w_sheet['B2'] = "Location in mm"
                w_sheet['C2'] = "Total Value ="
                w_sheet['D2'] = self.loclst.length
                
                for k in range(self.loclst.length):
                    w_sheet.cell(k+3,1, self.loclst.loclst[1][k])
                    w_sheet.cell(k+3,2, self.loclst.loclst[0][k])
                    w_sheet.cell(k+3,3, self.loclst.loclst[2][k])
                    
            elif many:
                lst_loclst = many
                for k in range(len(lst_loclst)):
                    sheet_name = "Sheet" + str(k+1)
                    w_sheet = wb.create_sheet(sheet_name)
                
                    w_sheet['A1'] = "Data from the Nulling System"
                    w_sheet['A2'] = "Time"
                    w_sheet['B2'] = "Location in mm"
                    w_sheet['C2'] = "Total Value ="
                    w_sheet['D2'] = lst_loclst[k].length
                    
                    for i in range(lst_loclst[k].length):
                        w_sheet.cell(i+3,1, lst_loclst[k].loclst[1][i])
                        w_sheet.cell(i+3,2, lst_loclst[k].loclst[0][i])
                        w_sheet.cell(i+3,3, lst_loclst[k].loclst[2][i])
                
            wb.save(path)
        
        else:
            wb = xl.load_workbook("CurrentDataSave.xlsx")
            
            if not many:
                wb_sheet_names = wb.sheetnames
                w_sheet = wb[wb_sheet_names[0]]
                w_sheet['D2'] = self.loclst.length
                for k in range(self.loclst.length):
                    w_sheet.cell(k+3,1, self.loclst.loclst[1][k])
                    w_sheet.cell(k+3,2, self.loclst.loclst[0][k])
                    w_sheet.cell(k+3,3, self.loclst.loclst[2][k])
                    
            elif many:
                lst_loclst = many
                wb_sheet_names = wb.sheetnames
                for k in range(len(lst_loclst)):
                    try:
                        w_sheet = wb[wb_sheet_names[k]]
                    except:
                        sheet_name = "Sheet" + str(k+1)
                        w_sheet = wb.create_sheet(sheet_name)
                    
                    w_sheet['D2'] = lst_loclst[k].length
                    
                    for i in range(lst_loclst[k].length):
                        w_sheet.cell(i+3,1, lst_loclst[k].loclst[1][i])
                        w_sheet.cell(i+3,2, lst_loclst[k].loclst[0][i])
                        w_sheet.cell(i+3,3, lst_loclst[k].loclst[2][i])
                        
            wb.save("CurrentDataSave.xlsx")
                
    
        
    def getInstr(self):
        
        """Return the opened resource currently configure"""
        
        return (self.arduino, self.calmotor)
    
    
    def getLstChoice(self):
        
        """Return a list of all the current temp data (Saved in temp)"""
        
        with open("temp/TempCount.txt", "r") as txt:
            s = txt.readlines()
            
        lst_loclst = []
        for k in s:
            file_name = k.replace("\n","")
            loclst = locationlst()
            #print("reading : " + file_name)
            with open(file_name, "r") as txt:
                r = txt.readlines()
                for r_line in r:
                    r_lst = r_line.split(";")
                    loclst.loclst = np.append(loclst.loclst, [[float(r_lst[0])],[float(r_lst[1])],[int(float(r_lst[2].replace("\n","")))]], axis = 1)
                lst_loclst.append([file_name, loclst])
            
        return lst_loclst
                


    def getRmList(self):
        
        """Return the list of all the resources founded during configuration"""
        
        return self.rm_list
    
    
    def nullinglvdt(self):
        
        """Set the null position of the LVDT. Call lib.nulllvdt."""
        
        nl.nulllvdt(self.task, self.lvdtName)
        
    
    def plotData(self, plotResult):
        
        plotResult.plot([self.loclst])
        
    def printError(self, e):
        
        """Print an error in Error.txt"""
        
        with open("lib/Error.txt", "a") as txt:
            for i in e:
                txt.write(i + "\t")
    
    
    def resetArduino(self):
        
        """Reset the motor position"""
        
        e = cm.resetSys(self.arduino)
        if type(e) != bool:
            error = ""
            for k in range(len(e)):
                error += e[k]
            return error
        else:
            return True
        
    
    def resetCalMotor(self):
        
        """Reset the Cal Motor position"""
        
        ca.resetCal(self.calmotor)
    
    
    def resetloclst(self):
        
        self.loclst.clear()
        
        
    def runSysLoc(self, tloc):
        
        """Run the system to the given location"""
        
        e = cm.runSysLoc(self.arduino, tloc = tloc)
        if type(e) == list:
            return e
        else:
            return True
        
        
    def runSysMeasure(self, gui, plotResult, nb_time):
        
        """Run the calibration programm of the system"""
        
        self.loclst.clear()
        #Check of direction
        d = self.arduino.query("dir")
        #print(d)
        if d == '0':
            self.arduino.query("dirs")
        #Go
        sec = time.time()
        self.loclst.loclst = np.append(self.loclst.loclst, [[float(self.arduino.query("loc"))], [0], [0]],  axis = 1)
        self.loclst.length += 1
        run.runMeasure(self, gui, plotResult, sec, nb_time)
        
        
    def runSysCal(self, gui, plotResult, nb_time):
        
        """Run the calibration programm of the system"""
        
        self.loclst.clear()
        #Check of direction
        d = self.arduino.query("dir")
        #print(d)
        if d == '0':
            self.arduino.query("dirs")
        #Go
        sec = time.time()
        self.loclst.loclst = np.append(self.loclst.loclst, [[float(self.arduino.query("loc"))], [0], [0]],  axis = 1)
        self.loclst.length += 1
        run.runCal(self, gui, plotResult, sec, nb_time)

   
    def saveData(self):
        
        """Save the data in a temporary file SaveDataTemp.txt"""
        
        date = str(datetime.datetime.now())
        date = date.replace(":","_")
        file_name = "temp/" + str(date) + ".txt"
        #print(file_name)
        with open(file_name, "w") as txt:
            for k in range(self.loclst.length):
                txt.write(str(self.loclst.loclst[0][k]) + ";" + str(self.loclst.loclst[1][k]) +  ";" + str(self.loclst.loclst[2][k]) + "\n")
            
        with open("temp/TempCount.txt", "a") as txt:
            txt.write(file_name + "\n")

    
    def setMass(self, n):
        if self.mass_count.count < n:
            while self.mass_count.count < n:
                ca.goStepCal(self.calmotor)
                self.mass_count.count += 1
        elif self.mass_count.count > n:
            while self.mass_count.count > n:
                ca.goStepCal(self.calmotor, True)
                self.mass_count.count -= 1
    

    def stopSys(self, gui):
        
        """Stop the calibration programm of the system"""
        
        #print(1)
        
        self.arduino.query("STOP")
        if self.temp.mark:
            gui.after_cancel(self.temp.mark)
            self.temp.mark = None
        #We clean both buffer
        try:
            self.arduino.query("*IDN?")
        except:
            pass
        empty = False
        while not empty:
            try:
                self.arduino.read()
            except:
                empty = True
        try:
            self.arduino.clear()
            self.calmotor.clear()
        except:
             pass  
        self.temp.resettemp()
        self.resetCalMotor()
        self.resetArduino()
        self.saveData()
        tk.messagebox.showinfo("Result", "Movment Done")
        #print(self.loclst.loclst)
                
                
    def testSys(self):
        
        """Test the System"""
        
        test.testSys(self.arduino, self.calmotor, self.task, self.res)
          
        
        
 ###############################################################################
        
class response():
    
    
    """Transport the response of the test"""
    
    
    def __init__(self):
        
        self.res1 = False
        self.res2 = False
        self.res3 = False
   
 ###############################################################################

class locationlst():
    
    
    """Contain the location list"""
    
    
    def __init__(self):
        
        """Array contain: [[location],[time],[if valid diff]]"""
        
        self.loclst = np.array([[],[],[]])
        self.length = 0
        
    def clear(self):
        
        self.loclst = np.array([[],[],[]])
        self.length = 0
    
 ###############################################################################   
 
class dataSheet():
    
    
    """Contain all the data of the system"""
    
    
    def __init__(self, instr):
        
        
        self.name_data = ["home_speed", "home_return", "dpr", "g", "mass", "rhoL", "a", "b", "h", "l"]
        self.nb_data = 10

        #Arduino data (will be updated, will be saved, can be update manually)
        #self.nb_data_arduino = 3
        
        self.home_speed = "0.0"
        self.home_return = "0.0"
        self.dpr = "0.0"
        
        #System data (will be saved, can be updated manually)
        #self.nb_data_system = 7
        
        self.g  = "9.810665"
        self.mass = "29.42" # in gramm
        self.rhoL = "0.1" # in gramm/m
        self.a = "0.0"
        self.b = "0.0"
        self.h = "0.0"
        self.l = "0.0"
        
        self.autoUpdateData(instr)
        

    def autoUpdateData(self, instr):
          
        """Auto update of the data of the arduino"""
        
        error = []
        try:
            s = instr.query("hs")
            if s.startswith("Error"):
                error.append(s)
            else:
                self.home_speed = s
                
            s = instr.query("hr")
            if s.startswith("Error"):
                error.append(s)
            else:
                self.home_return = s
                
            s = instr.query("dpr")
            if s.startswith("Error"):
                error.append(s)
            else:
                self.dpr = s
        except:
            error.append("ErrorQuery")
        if len(error) > 0:
            return error
        return True
            
    
    def updateArduino(self, instr):
        
        """Update arduino of changes are made"""
        
        error = []
        try:
            s = instr.query("hs" + self.home_speed)
            if s.startswith("Error"):
                error.append(s)
            else:
                self.home_speed = s
                
            s = instr.query("hr" + self.home_return)
            if s.startswith("Error"):
                error.append(s)
            else:
                self.home_speed = s
                
            s = instr.query("dpr" + self.dpr)
            if s.startswith("Error"):
                error.append(s)
            else:
                self.home_speed = s
                
        except:
            error.append("ErrorQuery")
        if len(error) > 0:
            return error
        return True
            
            