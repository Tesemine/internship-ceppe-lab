# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 13:26:00 2020

@author: tesemine
"""
#import lib.comlv as cl
import lib.comard as cm
import lib.comcal as ca
import time
import numpy as np
import random
#import tkinter as tk

class temp():
    
    
    """Contain all the variable used during a run"""
    
    
    def __init__(self):

        self.valZero = getZero()    #Value of the LVDT Nulling point
        self.H = False              #Keep track of the direction of the run
        self.running = False        #Keep track of the status of the system
        self.s = 0                  #Keep track of the current speed
        self.ns = 0                 #Keep track of the new calculated speed
        self.sup = False            #Sign of the difference
        self.mark = None            #Used in the GUI, keep the function running
        self.count = 0              #Keep track of the number of iteration
        self.check_diff = 0         #Keep track of the number of time the diff was small enougth
        self.iteration = 0          #Keep track of the number of time the entier process as been done
        self.must_reset = False     #Does the system need to reset itself
        self.listOfPause = [800,1000,1200,1400,1600]
        self.aChange = False
        
    def resettemp(self):
        
        """If called, reset all variable to their original state"""
        
        self.valZero = getZero()
        self.H = False
        self.running = False
        self.s = 0
        self.ns = 0
        self.sup = False
        self.mark = None
        self.count = 0
        self.check_diff = 0
        self.iteration = 0
        self.must_reset = False
        self.listOfPause = [800,1000,1200,1400,1600]
        self.aChange = False
        
  
def runMeasure(fonct, gui, plotResult, reftime, nb_time):
    
    """Run the measure of the thruster"""
    
    if fonct.temp.aChange:
        fonct.arduino.query("RESUME")
        fonct.temp.aChange = False
    
    listvar = fonct.task.read(number_of_samples_per_channel = 1000)
    var = 0   
    for k in range(1000):
        var += listvar[k]
    var = var/1000
        #Calculate the difference % to 0
    diff = fonct.temp.valZero - var
    
    if fonct.temp.check_diff > 80:
        fonct.temp.iteration += 1
        if fonct.temp.iteration == nb_time:
                fonct.stopSys(gui)
                return
        fonct.arduino.query("STOP")
        empty = False
        while not empty:
            try:
                fonct.arduino.read()
            except:
                empty = True
        fonct.resetArduino()
        fonct.temp.check_diff = 0
        
    
    oldsup = fonct.temp.sup
    if diff > 0:
        fonct.temp.sup = True
    else:
        fonct.temp.sup = False
    #print(diff, var)
    
    if not fonct.temp.running: #We run it
            #Choice of the direction
        if fonct.temp.sup: 
            temp.H = True
                
            #Choice of the speed
        fonct.temp.s = chooseSpeed(diff)
  
        a = cm.runSysCal(fonct.arduino, fonct.temp.s, fonct.temp.H)
        loc = float(fonct.arduino.query("loc"))
        if abs(diff) <= 0.001:
            fonct.loclst.loclst = np.append(fonct.loclst.loclst, [[loc], [time.time() - reftime], [1]],  axis = 1)
        else:
            fonct.loclst.loclst = np.append(fonct.loclst.loclst, [[loc], [time.time() - reftime], [0]],  axis = 1)
        fonct.loclst.length += 1
        plotResult.plot([fonct.loclst])
        
        if type(a) != bool:
            #print(a)
            return a
        fonct.temp.running = True     
        
    elif fonct.temp.running: #We check it
          
        fonct.temp.ns = chooseSpeed(diff)


        if fonct.temp.ns != fonct.temp.s:
            #print("CHANGE SPEED")
            fonct.arduino.query("s" + str(fonct.temp.ns))
            fonct.temp.s = fonct.temp.ns
            loc = float(fonct.arduino.query("loc"))
            if abs(diff) <= 0.001:
                fonct.loclst.loclst = np.append(fonct.loclst.loclst, [[loc], [time.time() - reftime], [1]],  axis = 1)
            else:
                fonct.loclst.loclst = np.append(fonct.loclst.loclst, [[loc], [time.time() - reftime], [0]],  axis = 1)
            fonct.loclst.length += 1
            plotResult.plot([fonct.loclst])
            fonct.temp.aChange = True
            
        if oldsup != fonct.temp.sup:
            #print("CHANGE DIR")
            fonct.arduino.query("dirs")
            loc = float(fonct.arduino.query("loc"))
            if abs(diff) <= 0.001:
                fonct.loclst.loclst = np.append(fonct.loclst.loclst, [[loc], [time.time() - reftime], [1]],  axis = 1)
            else:
                fonct.loclst.loclst = np.append(fonct.loclst.loclst, [[loc], [time.time() - reftime], [0]],  axis = 1)
            fonct.loclst.length += 1
            plotResult.plot([fonct.loclst])
            fonct.temp.aChange = True

    if abs(diff) < 0.0005:
        fonct.temp.check_diff += 1
        #print("DIFF+1 " + str(temp.check_diff))
        
    fonct.temp.count += 1
        
    if fonct.temp.aChange:
        fonct.arduino.query("PAUSE")
        fonct.temp.mark = gui.after(fonct.temp.listOfPause[random.randint(0,4)], lambda: runMeasure(fonct, gui, plotResult, reftime, nb_time))
    else:  
        fonct.temp.mark = gui.after(5, lambda: runMeasure(fonct, gui, plotResult, reftime, nb_time))

          
def runCal(fonct, gui, plotResult, reftime, nb_time):
    
    """
    fonct = functionInstr object, gui = Tk() obj, plotResult = plotResult object
                        reftime = Time float, nb_time = int
    First part of the run process. 
    """
        
    if fonct.temp.aChange:
        fonct.arduino.query("RESUME")
        fonct.temp.aChange = False
    
    if fonct.temp.check_diff > 80:                  #if we have enougth values
        #print(mass_count.count)
        #print(mass_count.dir)
        if fonct.mass_count.dir:                    #if the masses are giong down
            if fonct.temp.iteration == nb_time:     #if we reached asked nb of iteration
                fonct.stopSys(gui)                  #End of the procedure
                return
            ca.goStepCal(fonct.calmotor)            #Otherwise, we drop 1 mass
            fonct.mass_count.count += 1             #And we keep track of it
            if fonct.mass_count.count == 3:         #if we dropped 3
                fonct.mass_count.dir = False        #we invert the process
                fonct.temp.must_reset = True        #and we note to reset at first ascending turn
        elif not fonct.mass_count.dir:              #if the masses are going up
            if fonct.temp.must_reset:               #if it's first turn we reset
                #print("reset")
                fonct.arduino.write("STOP")
                empty = False
                while not empty:
                    try:
                        fonct.arduino.read()
                    except:
                        empty = True
                e = fonct.resetArduino()
                if type(e) != bool:
                    #print(e)
                    return e
                fonct.temp.must_reset = False
                fonct.temp.running = False
            else:                                   #Otherwise
                ca.goStepCal(fonct.calmotor, True)  #We put 1 mass up
                fonct.mass_count.count -= 1         #keep track of it
                if fonct.mass_count.count == 0:     #If all up ,1 iteration complete
                    fonct.mass_count.dir = True
                    fonct.temp.iteration += 1
        fonct.temp.check_diff = 0
        
    listvar = fonct.task.read(number_of_samples_per_channel = 1000)
    var = 0   
    for k in range(1000):
        var += listvar[k]
    var = var/1000
        #Calculate the difference compared to the zero
    diff = fonct.temp.valZero - var
        
    
    oldsup = fonct.temp.sup
    if diff > 0:
        fonct.temp.sup = True
    else:
        fonct.temp.sup = False
    #print(diff, var)
    
    if not fonct.temp.running: #We run it
            #Choice of the direction
        if fonct.temp.sup: 
            temp.H = True
                
            #Choice of the speed
        fonct.temp.s = chooseSpeed(diff)    
        
  
        a = cm.runSysCal(fonct.arduino, fonct.temp.s, fonct.temp.H)
        loc = float(fonct.arduino.query("loc"))
        if abs(diff) <= 0.001:
            fonct.loclst.loclst = np.append(fonct.loclst.loclst, [[loc], [time.time() - reftime], [1]],  axis = 1)
        else:
            fonct.loclst.loclst = np.append(fonct.loclst.loclst, [[loc], [time.time() - reftime], [0]],  axis = 1)
        fonct.loclst.length += 1
        plotResult.plot([fonct.loclst])
        
        if type(a) != bool:
            #print(a)
            return a
        fonct.temp.running = True
            
        
    elif fonct.temp.running: #We check it
           
        fonct.temp.ns = chooseSpeed(diff) #Choice of the new speed

        if fonct.temp.ns != fonct.temp.s:
            #print("CHANGE SPEED " + str(fonct.temp.ns))
            fonct.arduino.query("s" + str(fonct.temp.ns))
            fonct.temp.s = fonct.temp.ns
            loc = float(fonct.arduino.query("loc"))
            if abs(diff) <= 0.001:
                fonct.loclst.loclst = np.append(fonct.loclst.loclst, [[loc], [time.time() - reftime], [1]],  axis = 1)
            else:
                fonct.loclst.loclst = np.append(fonct.loclst.loclst, [[loc], [time.time() - reftime], [0]],  axis = 1)
            fonct.loclst.length += 1
            plotResult.plot([fonct.loclst])
            fonct.temp.aChange = True
            
        if oldsup != fonct.temp.sup:
            #print("CHANGE DIR")
            fonct.arduino.query("dirs")
            loc = float(fonct.arduino.query("loc"))
            if abs(diff) <= 0.001:
                fonct.loclst.loclst = np.append(fonct.loclst.loclst, [[loc], [time.time() - reftime], [1]],  axis = 1)
            else:
                fonct.loclst.loclst = np.append(fonct.loclst.loclst, [[loc], [time.time() - reftime], [0]],  axis = 1)
            fonct.loclst.length += 1
            plotResult.plot([fonct.loclst])
            fonct.temp.aChange = True

    if abs(diff) < 0.0005:
        fonct.temp.check_diff += 1
        #print("DIFF+1 " + str(temp.check_diff))
        
    fonct.temp.count += 1
        
    if fonct.temp.aChange:
        fonct.arduino.query("PAUSE")
        fonct.temp.mark = gui.after(fonct.temp.listOfPause[random.randint(0,4)], lambda: runCal(fonct, gui, plotResult, reftime, nb_time))
    else:  
        fonct.temp.mark = gui.after(5, lambda: runCal(fonct, gui, plotResult, reftime, nb_time))

        
        
def getZero():
    with open("./temp/lvdtnull.txt", "r") as txt:
        return (float(txt.read()))
    
def isZero(val, valzero):
    if valzero > val[0]:
        return True
    for k in range(0, len(val)-1):
        if valzero <= val[k] and valzero >= val[k+1]:
            return True
        
def isZeroH(val, valzero):
    if valzero < val[0]:
        return True
    for k in range(1, len(val)):
        if valzero <= val[k-1] and valzero >= val[k]:
            return True
        
def chooseSpeed(diff):
    """Given the diffrence input, return a certain speed"""
    
    if abs(diff) > 1:
        return 19     
    elif abs(diff) > 0.6:
        return 18
    elif abs(diff) > 0.3:
        return 16
    elif abs(diff) > 0.15:
        return 13
    elif abs(diff) > 0.1:
        return 10
    elif abs(diff) > 0.05:
        return 5         
    elif abs(diff) > 0.02:
        return 2
    elif abs(diff) <= 0.02:
        return 1