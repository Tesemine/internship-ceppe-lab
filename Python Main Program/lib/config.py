# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 15:14:21 2020

@author: tesemine
"""

import tkinter as tk
import pyvisa as pv


class config(tk.Tk):
    
    
    """
    GUI and all operation needed to configure the system.
    """


    def __init__(self, res):
            
            """GUI to choose the diffrent entry of the computer"""    
            
            tk.Tk.__init__(self)
            #self.geometry("600x500")
            self.title("Configuration")
            
            self.rm = pv.ResourceManager()
            self.rmlist = self.rm.list_resources()
            
            self.numvar1 = tk.IntVar()
            self.numvar2 = tk.IntVar()
            self.strvar = ["",""]
            self.lvdt = ""
            
            self.var1 = tk.StringVar()
            self.var1.set("Dev1")
            self.var2 = tk.StringVar()
            self.var2.set("ai0")
            
            frame1 = tk.Frame(self)
            frame2 = tk.Frame(self)
            frame3 = tk.Frame(self)
            frame4 = tk.Frame(self)
            
            tk.Label(frame1, text = "Configure the entry of the system", font = ('Arial', 18, "bold")).grid(row = 0, column = 1)
            #tk.Label(frame1, text = "(Exit the window after you choosed)").grid(row = 1, column = 1)
            frame1.grid_rowconfigure(2,minsize = 50)
            
            tk.Label(frame2, text = "Which detected entry is the Arduino Card?").grid(row = 0, column = 1)
            for k in range(len(self.rmlist)):
                tk.Radiobutton(frame2, text = self.rmlist[k], variable = self.numvar1, value = k).grid(row = k+1, column = 1)
                
            tk.Label(frame3, text = "Which detected entry is the Calibration Motor?").grid(row = 0, column = 1)
            for k in range(len(self.rmlist)):
                tk.Radiobutton(frame3, text = self.rmlist[k], variable = self.numvar2, value = k).grid(row = k+1, column = 1)
    
    
            frame4.grid_rowconfigure(0,minsize = 50)

            tk.Label(frame4, text = "Which NI Device is linked to the LVDT (Default = Dev1)").grid(row = 1, column = 1)
            tk.Entry(frame4, textvariable = self.var1).grid(row = 2, column = 1)

            tk.Label(frame4, text = "On this device, on which port is the LVDT connected ? (Default = ai0)").grid(row = 3, column = 1)
            tk.Entry(frame4, textvariable = self.var2).grid(row = 4, column = 1)

            
            frame1.grid(row = 0)
            frame2.grid(row = 1)
            frame3.grid(row = 2)
            frame4.grid(row = 3)
            
            tk.Button(self, text = "Enter", command =lambda: self.verif(res)).grid(row = 4, pady = 5)
            tk.Button(self, text = "Enter without testing", command =lambda: self.verif(res, cond = True)).grid(row = 5, pady = 5)
            tk.Button(self, text = "Exit", command =lambda: self.closeConfig(res)).grid(row = 6, pady = 5)
            tk.Label(self, text = "Note that if nothing appended when pressing OK, check lib\Error.txt").grid(row = 7)
      
        
    def verif(self, res, cond = False):
        
        """Check if valid argument are choosed"""
        
        if cond:
            res.res3 = True
        
        if self.numvar1.get() == self.numvar2.get():
            tk.messagebox.showwarning("Warning", "You can't chose the same entry for Arduino and the Calibration motor")
            return
        
        elif (not self.var1.get().startswith("Dev")) or (not self.var2.get().startswith("ai")):
            tk.messagebox.showwarning("Warning", "Invalid entry name for the LVDT")
        
        else:
            self.lvdt = self.var1.get() + "/" + self.var2.get()
            self.strvar[0] = self.rmlist[self.numvar1.get()]
            self.strvar[1] = self.rmlist[self.numvar2.get()]
            self.destroy()
            
            
    def closeConfig(self, res):
        
        res.res2 = True
        self.destroy()
    
        

   