# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 15:03:38 2020

@author: tesemine

Have all the methods needed to control the arduino
"""
import time


def resetSys(instr):
    
    """Reset the position of the Nulling system"""
    
    response = False
    i = 0
    empty = False
    error = []
    instr.write("reset")
    while (not response) and i < 100 :
        try:
            s = instr.read()
            if s == "Done":
                response = True
            elif s.startswith("Error"):
                error.append(s + " ")
                break
                            
        except:
            pass
        time.sleep(0.5)
        i+=1
    while not empty:
        try:
            instr.read()
        except:
            empty = True
    if i == 100 or response == False:
        error.append("ErrorTimeOut ")
        return error
    else:
        return True
    

def runSysCal(instr, s = 5, d = False):
    
    """instr = opended resources, s = int, d = boolean
    Run the motor in cal config for the given parameters"""
    
    error = []
    
    if d:
        m = instr.query("dirs")
        if m.startswith("Error"):
            error.append(m)
            return error
    
    m = instr.query("s" + str(s))
    if m.startswith("Error"):
        error.append(m)
        return error
    
    m = instr.query("runcal")
    if m.startswith("Error"):
        error.append(m)
        return error
    
    return True

def runSysLoc(instr, s = 16, tloc = "0.0"):
    
    """instr = opended resources, s = int, d = boolean
    Run the motor in cal config for the given parameters"""
    
    error = []
    response = False
    i = 0
    empty = False
    
    m = instr.query("s" + str(s))
    if m.startswith("Error"):
        error.append(m)
        return error
    
    m = instr.query("tloc" + tloc)
    if m.startswith("Error"):
        error.append(m)
        return error
    
    instr.write("runloc")
    while (not response) and i < 100 :
        try:
            s = instr.read()
            if s == "Done":
                response = True
            elif s.startswith("Error"):
                error.append(s + " ")
                break
        except:
            pass
        time.sleep(0.5)
        i+=1
    while not empty:
        try:
            instr.read()
        except:
            empty = True
    if i == 100 or response == False:
        error.append("ErrorTimeOut ")
        return error
    else:
        return True