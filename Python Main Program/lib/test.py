# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 11:19:28 2020

@author: tesemine
"""
import tkinter as tk
import time


class testBox():
    
    
    """
    GUI and test Programm of the entire system. Must be runned before any use of the Nulling System.
    Note that this GUI is refrshing itself as the procedure goes on
    Contain the following methods: 
    |    
    |    __init__(self, pyvisa.resources.serial.SerialInstrument, nidaqmx.task.Task, function.res)
    |        - Initiate GUI and launch of the testing programm
    |    
    |    refresh(self, function.res)
    |        - The refresh() method refresh the GUI and do the testing at the same time
    |        
    |    printResult(self)
    |        - Print the result of the test in ErrorTrace.txt
    |
    |    sendRes(self, function.res)
    |        - Return the option chooses once the test finished, in a res object
    """
    
    
    def __init__(self, instrument, motor, tasks, res):
        
        """
        instrument = SerialInstrument from pyvisa package
        tasks = opened task from the nidaqmx package
        res = object for the res class defined in fonction.py
        
        Initiate GUI and launch of the testing programm"""
        
        self.root = tk.Tk()
        #self.root.geometry("500x400")
        self.root.title("Testing")
        
        
        self.lstd = [0 for k in range(8)]
        self.lste = [[""] for k in range(8)]
        
        self.i = 0
        self.repeat = True
        
        self.instr = instrument
        self.motor = motor
        self.task = tasks
        
        self.lst = ["Arduino : Connection", "Arduino : Data      ", "LVDT : Connection   ", "Motor : Movment Cal ", "Motor : Movment Loc ", "LDVT : Acquisition  ", "Calibration Motor   ", "End : Reset         "]
        
        tk.Label(self.root, text = "Testing the system", font = ('Arial', 18, "bold")).grid(row = 0, column = 1)
        tk.Label(self.root, text = "Wait while the system is being tested").grid(row = 1, column = 1)
        self.table = tk.Frame(self.root)
        tk.Label(self.table, text = self.lst[0], width = 30).grid(row = 0, column = 0)
        tk.Label(self.table, text = self.lst[1], width = 30).grid(row = 1, column = 0)
        tk.Label(self.table, text = self.lst[2], width = 30).grid(row = 2, column = 0)
        tk.Label(self.table, text = self.lst[3], width = 30).grid(row = 3, column = 0)
        tk.Label(self.table, text = self.lst[4], width = 30).grid(row = 4, column = 0)
        tk.Label(self.table, text = self.lst[5], width = 30).grid(row = 5, column = 0)
        tk.Label(self.table, text = self.lst[6], width = 30).grid(row = 6, column = 0)
        tk.Label(self.table, text = self.lst[7], width = 30).grid(row = 7, column = 0)
        self.c0 = tk.Label(self.table, text = "-", width = 30)
        self.c1 = tk.Label(self.table, text = "-", width = 30)
        self.c2 = tk.Label(self.table, text = "-", width = 30)
        self.c3 = tk.Label(self.table, text = "-", width = 30)
        self.c4 = tk.Label(self.table, text = "-", width = 30)
        self.c5 = tk.Label(self.table, text = "-", width = 30)
        self.c6 = tk.Label(self.table, text = "-", width = 30)
        self.c7 = tk.Label(self.table, text = "-", width = 30)
        
        self.lstc = [self.c0, self.c1, self.c2, self.c3, self.c4, self.c5, self.c6, self.c7]

        self.c0.grid(row = 0, column = 1)
        self.c1.grid(row = 1, column = 1)
        self.c2.grid(row = 2, column = 1)
        self.c3.grid(row = 3, column = 1)
        self.c4.grid(row = 4, column = 1)
        self.c5.grid(row = 5, column = 1)
        self.c6.grid(row = 6, column = 1)
        self.c7.grid(row = 7, column = 1)

        self.table.grid(row = 2, column = 1)
        self.table.grid_columnconfigure(1, minsize = 200)
        tk.Label(self.root, text = "Refer to the Manual if Errors are found").grid(row = 3, column = 1)
        self.root.after(1000, lambda: self.refresh(res))

        
    def refresh(self, res):
        
        """
        res = object for the res class defined in fonction.py
        The refresh() method refresh the GUI and do the testing at the same time"""
        
        #print(self.i)
        if self.i == 0:
            try:
                self.instr.query("")
            except:
                pass
            try:
                empty = False
                while not empty:
                        try:
                            self.instr.read()
                        except:
                            empty = True
                s = self.instr.query("*IDN?")
                if s.startswith("Error"):
                    self.lste[0].append(s + " ")
                    self.lstd[0] = -1
                elif s == "Arduino":
                    self.lstd[0] = 1
                else:
                    self.lste[0].append("ErrorResultQuery")
                    self.lstd[0] = -1
            except:
                self.lstd[0] = -1
                self.lste[0].append("ErrorQuery ")
                
            if self.lstd[0] == 1:
                self.c0.configure(text = "Done")            
            elif self.lstd[0] == -1:
                self.c0.configure(text = "Error")
            
        if self.i == 1:   
            state = 0
            if self.lstd[0] > -1:
                try:
                    self.instr.write("idleinfo")
                    s = self.instr.read()
                    if s.startswith("Error"):
                        self.lstd[1] = -1
                        self.lste[1].append(s)
                    else:
                        while(s != ""):
                            if s == "Homed":
                                state = 1
                            elif s == "Ended":
                                state = -1
                            s = self.instr.read()
                        self.lstd[1] = 1
                except:
                    self.lstd[1] = -1
                    self.lste[1].append("ErrorReading ")
                    
            else:
                self.lstd[1] = -1
                self.lste[1].append("ErrorStepOneIncomplete")
                
            if self.lstd[1] == 1:
                self.c1.configure(text = "Done")
            elif self.lstd[1] == -1:
                self.c1.configure(text = "Error")
           
        if self.i == 2:
            try:
                self.task.read()
                self.lstd[2] = 1
            except:
                self.lstd[2] = -1
                self.lste[2].append("ErrorReadingLVDT ")
                
            if self.lstd[2] == 1:
                self.c2.configure(text = "Done")
            elif self.lstd[2] == -1:
                self.c2.configure(text = "Error")
            
        if self.i == 3:
            if self.lstd[0] > -1:
                response = False
                self.instr.write("reset")
                i = 0
                empty = False
                while (not response) and i < 60 :
                    try:
                        s = self.instr.read()
                        if s == "Done":
                            response = True
                        elif s.startswith("Error"):
                            self.lste[3].append(s + " ")
                            self.lstd[3] = -1
                            break
                            
                    except:
                        pass
                    time.sleep(0.5)
                    i+=1
                while not empty:
                    try:
                        self.instr.read()
                    except:
                        empty = True
                if i == 60 or response == False:
                    self.lstd[3] = -1
                    self.lste[3].append("ErrorTimeOut ")
                else:
                    response = False
                    empty = False
                    self.instr.query("s16")
                    self.instr.write("runcal")
                    time.sleep(4)
                    self.instr.query("STOP")
                    while (not response) and i < 40 :
                        try:
                            s = self.instr.read()
                            if s == "Done":
                                response = True
                            elif s.startswith("Error"):
                                self.lste[3].append(s + " ")
                                self.lstd[3] = -1
                                break
                        except:
                            pass
                        i+=1
                        time.sleep(0.5)
                    while not empty:
                        try:
                            self.instr.read()
                        except:
                            empty = True
                    if i == 40 or response == False:
                        self.lstd[3] = -1
                        self.lste[3].append("ErrorTimeOut ")
                    else:
                        response = False
                        self.instr.write("reset")
                        i = 0
                        empty = False
                        while (not response) and i < 60 :
                            try:
                                s = self.instr.read()
                                if s == "Done":
                                    response = True
                                elif s.startswith("Error"):
                                    self.lste[3].append(s + " ")
                                    self.lstd[3] = -1
                            except:
                                pass
                            time.sleep(0.5)
                            i+=1
                        while not empty:
                            try:
                                self.instr.read()
                            except:
                                empty = True
                        if i == 60 or response == False:
                            self.lstd[3] = -1
                            self.lste[3].append("ErrorTimeOut ")
                        else:
                            self.lstd[3] = 1
            else:
                self.lstd[3] = -1
                self.lste[3].append("ErrorStepOneIncomplete ")
                
            if self.lstd[3] == 1:
                self.c3.configure(text = "Done")
            elif self.lstd[3] == -1:
                self.c3.configure(text = "Error")
                
        if self.i == 4:  
            if self.lstd[0] > -1 and self.lstd[2] > -1:
                try:
                    avg = 0
                    lstlvdt = self.task.read(number_of_samples_per_channel = 100)
                    varlvdt = []
                    for k in lstlvdt:
                        avg += k
                    varlvdt.append(avg/100)
                    i = 0
                    response = False
                    empty = False
                    tloc = 100
                    loc = 0
                    self.instr.query("tloc" + str(tloc))
                    self.instr.query("s16")
                    #print(a,b)
                    self.instr.write("runloc")
                    time.sleep(5)
                    while (not response) and i < 60 :
                        avg = 0
                        lstlvdt = self.task.read(number_of_samples_per_channel = 100)
                        for k in lstlvdt:
                            avg += k
                        varlvdt.append(avg/100)
                        #print(avg/100)
                        try:
                            s = self.instr.read()
                            if s == "Done":
                                response = True
                            elif s.startswith("Error"):
                                self.lste[4].append(s + " ")
                                self.lstd[4] = -1
                                break
                        except:
                            pass
                        time.sleep(0.5)
                        i+=1
                    try:
                        loc = float(self.instr.read())
                        #print(loc)
                    except:
                        pass
                    if (tloc - loc) > 1:
                        self.lstd[4] = -1
                        self.lste[4].append("ErrorLocUnreached ")
                    while not empty:
                        try:
                            self.instr.read()
                        except:
                            empty = True
                    if i == 60 or response == False:
                        self.lstd[4] = -1
                        self.lstd[4].append("ErrorTimeOut ")
                    else:
                        self.lstd[4] = 1
                        m = 0
                        #print(varlvdt)
                        for k in range(1,len(varlvdt)):
                            dif = abs(varlvdt[k-1] - varlvdt[k])
                            if dif > m:
                                m = dif
                                #print(m)
                        if m > 0.2:
                            self.lstd[5] = 1
                        else:
                            self.lstd[5] = -1
                            self.lste[5].append("ErrorValueLVDT")
                except:
                    self.lstd[4] = -1
                    self.lste[4].append("ErrorCallProcess ")
                    self.lstd[5] = -1
                    self.lste[5].append("ErrorCallProcess ")
            else:
                 self.lstd[4] = -1
                 self.lste[4].append("ErrorStepOneOrTwoIncomplete ")
                 self.lstd[5] = -1
                 self.lste[5].append("ErrorStepOneOrTwoIncomplete ")
                 
            if self.lstd[4] == 1:
                self.c4.configure(text = "Done")
            elif self.lstd[4] == -1:
                self.c4.configure(text = "Error")
            
            if self.lstd[5] == 1:
                self.c5.configure(text = "Done")
            elif self.lstd[5] == -1:
                self.c5.configure(text = "Error")
        
        if self.i == 5:
            if self.lstd[5] > -1:
                try:
                    self.motor.write("MN")
                    self.motor.write("LD0")
                    self.motor.write("A10")
                    self.motor.write("V1")
                    self.motor.write("D-1500000")
                    self.motor.write("G")
                except:
                    self.lstd[6] = -1
                    self.lste[6].append("ErrorComMotor")
                if self.lstd[6] > -1:
                    time.sleep(10)
                    avg1 = 0
                    avg2 = 0
                    lstlvdt = self.task.read(number_of_samples_per_channel = 100)
                    for k in lstlvdt:
                        avg1 += k
                    avg1 = avg1/100
                    
                    for k in range(3):
                        self.motor.write("MN")
                        self.motor.write("LD0")
                        self.motor.write("A10")
                        self.motor.write("V1")
                        self.motor.write("D145000")
                        self.motor.write("G")
                        
                    time.sleep(15)
                    lstlvdt = self.task.read(number_of_samples_per_channel = 100)
                    for k in lstlvdt:
                        avg2 += k
                    avg2 = avg2/100
                    #print(avg2 - avg1)
                    if (avg2 - avg1) > 0.5:
                        self.lstd[6] = 1
                    else:
                        self.lstd[6] = -1
                        self.lste[6].append("ErrorMoveMotor")
                        
                    self.motor.write("MN")
                    self.motor.write("LD0")
                    self.motor.write("A10")
                    self.motor.write("V1")
                    self.motor.write("D-1500000")
                    self.motor.write("G")
                    
                    time.sleep(15)
            else:
                self.lstd[6] = -1
                self.lste[6].append("ErrorPreviousLVDT")
                
            if self.lstd[6] == 1:
                self.c6.configure(text = "Done")
            elif self.lstd[6] == -1:
                self.c6.configure(text = "Error")
               
        if self.i == 6: 
            if self.lstd[0] > -1:
                response = False
                self.instr.write("reset")
                i = 0
                empty = False
                while (not response) and i < 60 :
                    try:
                        s = self.instr.read()
                        if s == "Done":
                            response = True
                        elif s.startswith("Error"):
                            self.lste[7].append(s + " ")
                            self.lstd[7] = -1
                            break
                            
                    except:
                        pass
                    time.sleep(0.5)
                    i+=1
                while not empty:
                    try:
                        self.instr.read()
                    except:
                        empty = True
                if i == 60 or response == False:
                    self.lstd[7] = -1
                    self.lste[7].append("ErrorTimeOut ")
                else:
                    self.lstd[7] = 1
            if self.lstd[0] == 1 and  self.lstd[1] == 1 and self.lstd[2] == 1 and self.lstd[3] == 1 and self.lstd[4] == 1 and self.lstd[5] == 1 and self.lstd[6] == 1 and self.lstd[7] == 1:
                self.c7.configure(text = "Done")
            else:
                self.c7.configure(text = "Error")
                self.lstd[7] = -1
                self.lste[7].append("ErrorPreviousSteps ")


        self.i += 1
        
        if self.i == 7:
            self.repeat = False
            time.sleep(2)
            frame = tk.Frame(self.root)
            if self.c7.cget("text") == "Done":
                tk.Button(frame, text="Ok", command=lambda: self.sendRes(res, True)).grid(row = 0, column = 0)
                tk.Label(frame, text = "Press 'Ok' to launch the App").grid(row = 0, column = 2)
                tk.Button(frame, text="Print", command=self.printResult).grid(row = 2, column = 0)
                tk.Label(frame, text = "Press 'Print' to print a recap in a txt file").grid(row = 2, column = 2)
                
            else:    
                tk.Button(frame, text="Ok", command=lambda: self.sendRes(res, False)).grid(row = 0, column = 0)
                tk.Label(frame, text = "Press 'Ok' to close the app while you correct the errors").grid(row = 0, column = 2)
                tk.Button(frame, text="Force entry", command=lambda: self.sendRes(res, True)).grid(row = 1, column = 0)
                tk.Label(frame, text = "Press 'Force entry' if you still want to enter the App. Can cause issues").grid(row = 1, column = 2)
                tk.Button(frame, text="Print", command=self.printResult).grid(row = 2, column = 0)
                tk.Label(frame, text = "Press 'Print' to print a recap in a txt file").grid(row = 2, column = 2)
                
            frame.grid(row = 4, column = 1)
            self.root.update()
        if self.repeat:
            self.root.after(1000, lambda: self.refresh(res))


    def printResult(self):
        
        """Print the result of the test in Error.txt"""
        
        with open("lib\Error.txt", "w") as txt:
            for k in range(8):
                se = ""
                if self.lstd[k] == 1:
                    se = "None"
                else:
                    for j in self.lste[k]:
                        se = se + j
                txt.write(self.lst[k] + "     \t" + self.lstc[k].cget("text") + "\t    Exact Error: " + se + "\n")
 
    
    def sendRes(self, res, c):
        
        """Return the option chooses once the test finished, in a res object"""
        
        if c:
            res.res1 = c
        else:
            res.res1 = c
        self.root.destroy()


def testSys(arduino, motor, task, res):
    
    """Create the testing GUI and run it"""
    
    box = testBox(arduino, motor, task, res)
    box.root.mainloop()

