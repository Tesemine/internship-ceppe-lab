# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 13:58:01 2020

@author: tesemine
"""

def nulllvdt(task, device):
    with open("./temp/lvdtnull.txt", "w") as txt:
        var = task.read(number_of_samples_per_channel=100)
        avg = 0
        for k in var:
            avg += k
        txt.write(str(avg/len(var)))