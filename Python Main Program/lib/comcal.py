# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 11:17:03 2020

@author: tesemine

Have all the methods needed to control the calibration motor
"""

import time 

class stepCal():
    
    
    """Use to count the masses applied by the calibration motor"""
    
    
    def __init__(self):
        self.count = 0
        self.dir = True
        
        

def resetCal(instr):
    
    """Reset the Cal motor position"""
    
    instr.write("MN")
    instr.write("LD0")
    instr.write("A10")
    instr.write("V1")
    instr.write("D-1500000")
    instr.write("G")
    
    time.sleep(5)
    return True
    
    
def goStepCal(instr, H = False):
    
    instr.write("MN")
    instr.write("LD0")
    instr.write("A10")
    instr.write("V1")
    if H:
        instr.write("D-145000")
    else:
        instr.write("D145000")
    instr.write("G")

