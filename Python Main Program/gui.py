# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 17:45:25 2020

@author: theo2
"""


import matplotlib
matplotlib.use('TkAgg')

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure

import tkinter as tk
import time
#import os.path
#import numpy as np
#import pyvisa as pv
import os
#import nidaqmx as nx



class Root(tk.Tk):
    
    
    """Class defining the main windows of the software, which can contain different pages"""
    
    
    def __init__(self, fonct):
        
        """Defining the class"""
        
        tk.Tk.__init__(self)
        self._frame = None
        tk.Label(self, text = "Last Update: %s" % time.ctime(os.path.getmtime("main.py"))).pack(side = "bottom", fill = "both")
        tk.Button(self, text = "Quit", command = self.quit).pack(side = "bottom")
        self.switchFrame(StartPage, fonct)
        
    def switchFrame(self, frame_class, fonct):
        
        """Method used to switch to another page in th window"""
        
        new_frame = frame_class(self, fonct)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        self._frame.pack(side = "top", fill = "both")
        
    def quit(self):
        self.destroy()
        
        
class StartPage(tk.Frame):
    
    
    """Classe defining the StartPage of the Software"""
    
    def __init__(self, master, fonct):
        
        """defining the class"""
        
        self.page = tk.Frame.__init__(self, master)
        tk.Label(self, text = "Main Menu", font = ('Arial', 18, "bold")).pack(side = "top", fill = "both")
        txt = open("data/StartPage.txt", "r")
        tk.Label(self, text = txt.readline()).pack(side = "top", fill = "both")
        tk.Label(self, text = txt.readline()).pack(side = "top", fill = "both")
        tk.Label(self, text = txt.readline()).pack(side = "top", fill = "both")
        
        self.menu = tk.Frame(self, self.page)
        tk.Button(self.menu, text = "Test", command=lambda: master.switchFrame(TestPage, fonct)).grid(row = 0, column = 0, pady = 5)
        tk.Label(self.menu, text = txt.readline()).grid(row = 0, column = 1, sticky = "W", pady = 5)
        tk.Button(self.menu, text = "Run", command=lambda: master.switchFrame(RunPage, fonct)).grid(row = 1, column = 0, pady = 5)
        tk.Label(self.menu, text = txt.readline()).grid(row = 1, column = 1, sticky = "W", pady = 5)
        tk.Button(self.menu, text = "Manual", command=lambda: master.switchFrame(ManualPage, fonct)).grid(row = 2, column = 0, pady = 5)
        tk.Label(self.menu, text = txt.readline()).grid(row = 2, column = 1, sticky = "W", pady = 5)
        tk.Button(self.menu, text = "Data Sheet", command=lambda: master.switchFrame(DataPage, fonct)).grid(row = 3, column = 0, pady = 5)
        tk.Label(self.menu, text = txt.readline()).grid(row = 3, column = 1, sticky = "W", pady = 5)
        self.menu.pack(side = "top")
        
        txt.close()
        
        
class TestPage(tk.Frame):
    
    
    """Classe defining the Test Page of the Software"""
  
    
    def __init__(self, master, fonct):
        
        """defining the class"""
        
        self.page = tk.Frame.__init__(self, master)
        tk.Label(self, text = "Test Menu", font = ('Arial', 18, "bold")).pack(side = "top", fill = "both")
        txt = open("data/TestPage.txt", "r")
        for k in range(6):
            tk.Label(self, text = txt.readline()).pack(side = "top", fill = "both")
        
        self.menu = tk.Frame(self, self.page)
        tk.Button(self.menu, text = "Test", command =lambda: self.buttonFonct(master, fonct)).grid(row = 0, column = 0, pady = 5)
        tk.Label(self.menu, text = txt.readline()).grid(row = 0, column = 2, pady = 5)
        self.menu.pack(side = "top", pady = 5)
        self.menu.grid_columnconfigure(0,minsize = 400)
        self.menu.grid_columnconfigure(1,minsize = 50)
        self.menu.grid_columnconfigure(2,minsize = 275)
        self.menu.grid_rowconfigure(1,minsize = 100)
        
        tk.Button(self, text = "Back", command=lambda: master.switchFrame(StartPage, fonct)).pack(side = "top", pady = 5)
        
        txt.close()
        
    
    def buttonFonct(self, master, fonct):
        #master.destroy()
        fonct.testSys()
        
    
        
class RunPage(tk.Frame):
    
    
    """Class defining the Run Page of the Software"""
    
    
    def __init__(self, master, fonct):
        
        """defining the class"""
        
        self.page = tk.Frame.__init__(self, master)
        tk.Label(self, text = "Run Menu", font = ('Arial', 18, "bold")).pack(side = "top", fill = "both")
        txt = open("data/RunPage.txt", "r")
        for k in range(3):
            tk.Label(self, text = txt.readline()).pack(side = "top", fill = "both")
        
        self.nb_time = tk.StringVar()
        self.nb_time.set("1")
        
        self.menu = tk.Frame(self, self.page)
        tk.Entry(self.menu, textvariable = self.nb_time).grid(row = 0, column = 0, pady = 5)
        tk.Button(self.menu, text = "Run Calibraton", command =lambda: self.runSysCal(fonct, master)).grid(row = 0, column = 2, pady = 5)
        tk.Label(self.menu, text = txt.readline()).grid(row = 0, column = 4, pady = 5)
        tk.Entry(self.menu, textvariable = self.nb_time).grid(row = 2, column = 0, pady = 5)
        tk.Button(self.menu, text = "Run Measure", command = lambda: self.runSysMeasure(fonct, master)).grid(row = 2, column = 2, pady = 5)
        tk.Label(self.menu, text = txt.readline()).grid(row = 2, column = 4, pady = 5)
        tk.Button(self.menu, text = "Stop System", command =lambda: fonct.stopSys(master)).grid(row = 4, column = 2, pady = 5)
        tk.Label(self.menu, text = txt.readline()).grid(row = 4, column = 4, pady = 5)
        tk.Button(self.menu, text = "Exploit Data", command =lambda: master.switchFrame(ResultPage, fonct)).grid(row = 8, column = 2, pady = 5)
        tk.Label(self.menu, text = txt.readline()).grid(row = 8, column = 4, pady = 5)
        self.menu.pack(side = "top", pady = 5)
        self.menu.grid_columnconfigure(0,minsize = 275)
        self.menu.grid_columnconfigure(1,minsize = 50)
        self.menu.grid_columnconfigure(2,minsize = 275)
        self.menu.grid_rowconfigure(1,minsize = 5)
        self.menu.grid_rowconfigure(3,minsize = 5)
        self.menu.grid_rowconfigure(5,minsize = 5)
        self.menu.grid_rowconfigure(7,minsize = 5)
        
        self.pltResult = plotResult(self)
        self.pltResult.pack(side = "top")
        
        self.button_data = tk.Frame(self, self.page)
        tk.Button(self.button_data, text = "Plot current Data", command=lambda: fonct.plotData(self.pltResult)).grid(row = 1, column = 0, pady = 5)
        tk.Button(self.button_data, text = "Export current Data", command=lambda: self.export(fonct)).grid(row = 1, column = 2, pady = 5)
        tk.Button(self.button_data, text = "Reset current Data", command= fonct.resetloclst).grid(row = 1, column = 4, pady = 5)
        self.button_data.pack(side = "top", pady = 5)
        self.button_data.grid_columnconfigure(1,minsize = 10)
        self.button_data.grid_columnconfigure(3,minsize = 10)
        
        tk.Button(self, text = "Back", command=lambda: master.switchFrame(StartPage, fonct)).pack(side = "top", pady = 5)
        
        txt.close()
        
    def runSysCal(self, fonct, master):
        #print(0)
        try:
            nb = int(self.nb_time.get())
        except:
            tk.messagebox.showerror("Error","Enter an positive integer")
        fonct.runSysCal(master, self.pltResult, nb)
            
    def runSysMeasure(self, fonct, master):
        try:
            nb = int(self.nb_time.get())
        except:
            tk.messagebox.showerror("Error","Enter an positive integer")
        fonct.runSysMeasure(master, self.pltResult, nb)
        
    def export(self, fonct):
        rep = tk.messagebox.askyesno("Save", "Do you want to save the data in a blank file? By default, the results will be saved in a existing excel file which is already configured")
        if rep:
            name = tk.simpledialog.askstring(title = "Save data", prompt = "Enter the file name (the file will be saved in the Python file)")
            fonct.exportData(name)
        else:
            fonct.exportData()
            
  
class ResultPage(tk.Frame):
    
    """Class defining the compare page, sun-page from RunPage"""
    
    def __init__(self, master, fonct):
        
        """defining the class"""
        
        self.page = tk.Frame.__init__(self, master)
        tk.Label(self, text = "Exploit result page", font = ('Arial', 18, "bold")).pack(side = "top", fill = "both")
        
        self.up_frame = tk.Frame(self, self.page)
        
        self.lst_choice_box = fonct.getLstChoice()
        self.lst_choice_loc_var = []
        self.lst_choice_loc_frame = tk.Frame(self.up_frame)
        for k in range(len(self.lst_choice_box)):
            self.lst_choice_loc_var.append(tk.IntVar())
            tk.Checkbutton(self.lst_choice_loc_frame, text = self.lst_choice_box[k][0], variable = self.lst_choice_loc_var[k], command = self.plot).grid(row = k)
        self.lst_choice_loc_frame.grid(row = 0, column = 1)
        
        self.pltResult = plotResult(self.up_frame)
        self.pltResult.grid(row = 0, column = 3)
        
        self.up_frame.pack(side = "top")
        
        tk.Button(self, text = "Export selected Data", command=lambda: self.export(fonct)).pack(side = "top", pady = 5)
        
        
        
        
        
        tk.Button(self, text = "Back", command=lambda: master.switchFrame(RunPage, fonct)).pack(side = "top", pady = 5)
      
        
    def export(self, fonct):
        lst_lstloc = self.selectedData()
        rep = tk.messagebox.askyesno("Save", "Do you want to save the data in a blank file? By default, the results will be saved in a existing excel file which is already configured")
        if rep:
            name = tk.simpledialog.askstring(title = "Save data", prompt = "Enter the file name (the file will be saved in the Python file)")
            fonct.exportData(name, many = lst_lstloc)
        else:
            fonct.exportData(many = lst_lstloc)  
    
    def plot(self):
        
        """Refresh the plot"""
        
        lst_lstloc = self.selectedData()
        self.pltResult.plot(lst_lstloc)
            
    def selectedData(self):
        
        """Return the selected data"""
        
        lst_lstloc = []
        for k in range(len(self.lst_choice_box)):
            if self.lst_choice_loc_var[k].get():
                #print(self.lst_choice_box[k][1].loclst)
                lst_lstloc.append(self.lst_choice_box[k][1])
        return lst_lstloc
        
          
  
class ManualPage(tk.Frame):
    
    
    """Class defining the Mamual Page of the Software"""
    
    
    def __init__(self, master, fonct):
        
        """defining the class"""
        
        self.tloc = tk.StringVar()
        self.mass = tk.StringVar()
        self.tloc.set("0.0")
        self.mass.set("0")
        
        self.page = tk.Frame.__init__(self, master)
        tk.Label(self, text = "Manual Menu", font = ('Arial', 18, "bold")).pack(side = "top", fill = "both")
        txt = open("data/ManualPage.txt", "r")
        for k in range(1):
            tk.Label(self, text = txt.readline()).pack(side = "top", fill = "both")
        
        self.menu = tk.Frame(self, self.page)
        
        topmenu = tk.Frame(self.menu)
        tk.Button(topmenu, text = "Nulling the LVDT", command = fonct.nullinglvdt).grid(row = 0, column = 0, pady = 5)
        tk.Button(topmenu, text = "Reseting the Arduino", command =lambda: self.resetArduino(fonct)).grid(row = 0, column = 2, pady = 5)
        tk.Button(topmenu, text = "Reseting the Cal motor", command = fonct.resetCalMotor).grid(row = 0, column = 4, pady = 5)
        
        topmenu.pack(side = "top", pady = 5)
        topmenu.grid_columnconfigure(1,minsize = 10)
        topmenu.grid_columnconfigure(3,minsize = 10)
        
        bottommenu = tk.Frame(self.menu)
        tk.Entry(bottommenu, textvariable = self.tloc).grid(row = 0, column = 1, pady = 5)
        tk.Button(bottommenu, text = "Go to this distance", command =lambda: self.setDistance(fonct)).grid(row = 0, column = 3, pady = 5)
        tk.Label(bottommenu, text = txt.readline()).grid(row = 0, column = 5, pady = 5)
        tk.Entry(bottommenu, textvariable = self.mass).grid(row = 2, column = 1, pady = 5)
        tk.Button(bottommenu, text = "Put this number of mass", command =lambda: self.setMass(fonct)).grid(row = 2, column = 3, pady = 5)
        tk.Label(bottommenu, text = txt.readline()).grid(row = 2, column = 5, pady = 5)
        
        bottommenu.pack(side = "top")
        bottommenu.grid_columnconfigure(0,minsize = 10)
        bottommenu.grid_columnconfigure(2,minsize = 10)
        bottommenu.grid_columnconfigure(4,minsize = 10)
        
        self.menu.pack(side = "top", pady = 5)
        
        tk.Button(self, text = "Back", command=lambda: master.switchFrame(StartPage, fonct)).pack(side = "top", pady = 5)
        
        txt.close()
        
    def setDistance(self, fonct):
        e = fonct.runSysLoc(self.tloc.get())
        if type(e) != bool:
            tk.messagebox.showerror("Error", e)
        else:
            tk.messagebox.showinfo("Result", "Done")
            
    def resetArduino(self, fonct):
        e = fonct.resetArduino()
        if type(e) != bool:
            tk.messagebox.showerror("Error", e)
        else:
            tk.messagebox.showinfo("Result", "Done")
        
    def setMass(self, fonct):
        fonct.setMass(int(self.mass.get()))
        
   
#########################################################################################
class DataPage(tk.Frame):
    
    """Classe defining the Data Page of the Software"""
    
    
    def __init__(self, master, fonct):
        
        self.page = tk.Frame.__init__(self, master)       
        tk.Label(self, text  = "Data Sheet", font = ('Arial', 18, "bold")).pack(side = "top")
        
        self.lstvar = [tk.StringVar() for k in range(10)]
        self.lstvar[0].set(fonct.datasheet.home_speed)
        self.lstvar[1].set(fonct.datasheet.home_return)
        self.lstvar[2].set(fonct.datasheet.dpr)
        self.lstvar[3].set(fonct.datasheet.g)
        self.lstvar[4].set(fonct.datasheet.mass)
        self.lstvar[5].set(fonct.datasheet.rhoL)
        self.lstvar[6].set(fonct.datasheet.a)
        self.lstvar[7].set(fonct.datasheet.b)
        self.lstvar[8].set(fonct.datasheet.h)
        self.lstvar[9].set(fonct.datasheet.l)
        
        self.tab = tk.Frame(self, self.page)
        for k in range(fonct.datasheet.nb_data):
            tk.Entry(self.tab, textvariable = self.lstvar[k]).grid(row = k, column = 0)
            tk.Label(self.tab, text = fonct.datasheet.name_data[k]).grid(row = k, column = 1)
            
        self.tab.pack(side = "top")
        tk.Button(self, text = "Confirm", command =lambda: self.confirm(fonct)).pack(side = "top")
         
        tk.Button(self, text = "Back", command=lambda: master.switchFrame(StartPage, fonct)).pack(side = "top")
            
    def confirm(self, fonct):
            
        fonct.datasheet.home_speed = self.lstvar[0].get()
        fonct.datasheet.home_return = self.lstvar[1].get()
        fonct.datasheet.dpr = self.lstvar[2].get()
        fonct.datasheet.g = self.lstvar[3].get()
        fonct.datasheet.mass = self.lstvar[4].get()
        fonct.datasheet.rhoL = self.lstvar[5].get()
        fonct.datasheet.a = self.lstvar[6].get()
        fonct.datasheet.b = self.lstvar[7].get()
        fonct.datasheet.h = self.lstvar[8].get()
        fonct.datasheet.l = self.lstvar[9].get()
        
        fonct.datasheet.updateArduino(fonct.arduino)

################################################################################################

class plotResult(tk.Frame):
    
    """Create a plot of the result in a frame that can be intergrate in the main GUI"""
    
    def __init__(self, page):
        tk.Frame.__init__(self, page, bg = "blue")
        self.createWidgets()
        
    def createWidgets(self):
        self.fig = Figure(figsize = (15,5))
        self.ax = self.fig.add_axes([0.2,0.2,0.6,0.6])
        self.ax.set_xlabel("Time")
        self.ax.set_ylabel("Location in mm")
        self.ax.set_title("Follow the location here")
        
        self.canvas = FigureCanvasTkAgg(self.fig, master = self)
        self.canvas.get_tk_widget().pack(side = "bottom")
        
        self.toolbar = NavigationToolbar2Tk(self.canvas, self)
        self.toolbar.update()
        
        self.canvas.draw()
        
    def plot(self, lst_lstloc):
        self.ax.clear()
        for k in range(len(lst_lstloc)):
                self.ax.plot(lst_lstloc[k].loclst[1], lst_lstloc[k].loclst[0])
                for i in range(lst_lstloc[k].length):
                    if lst_lstloc[k].loclst[2][i] == 1:
                        self.ax.scatter(lst_lstloc[k].loclst[1][i], lst_lstloc[k].loclst[0][i], c = "red")
                    elif lst_lstloc[k].loclst[2][i] == 0:
                        self.ax.scatter(lst_lstloc[k].loclst[1][i], lst_lstloc[k].loclst[0][i], c = "blue")
        self.canvas.draw()
        

